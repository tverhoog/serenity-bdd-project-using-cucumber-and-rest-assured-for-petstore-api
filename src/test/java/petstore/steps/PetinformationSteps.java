package petstore.steps;

import cucumber.api.java.Before;
import io.restassured.RestAssured;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.given;
import static net.serenitybdd.rest.SerenityRest.then;

public class PetinformationSteps {

    @Before
    public void setDefaultURI() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @Step("Delete PetID {0}")
    public void deletePetID(int petID) {
        given()
                .pathParams("petID", petID).
                when()
                .delete("/pet/{petID}");
    }

    @Step("Create pet with ID {0}")
    public void createPet(int petID) {
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("id", petID);
        jsonAsMap.put("name", "GoodBoy");
        jsonAsMap.put("status", "available");
        jsonAsMap.put("photoUrls", new ArrayList<>(Collections.emptyList()));

        given().contentType("application/json")
                .body(jsonAsMap)
                .basePath("/pet")
                .when().post();
    }

    @Step("Response should be {0}")
    public void responseShouldBe(int code) {
        then()
                .assertThat()
                .statusCode(code);
    }

    @Step("Request pet with ID {0}")
    public void requestPet(int petID) {
        given()
                .pathParams("petID", petID).
                when()
                .get("/pet/{petID}");
    }
}

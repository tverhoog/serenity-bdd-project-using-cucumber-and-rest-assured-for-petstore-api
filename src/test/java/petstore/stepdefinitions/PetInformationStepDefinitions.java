package petstore.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import petstore.steps.PetinformationSteps;

public class PetInformationStepDefinitions {

    @Steps
    PetinformationSteps petinformationSteps;

    @Given("petID '{int}' does not exist")
    public void petidDoesNotExist(int petID) {
        petinformationSteps.deletePetID(petID);
    }

    @When("a pet with ID '{int}' is created")
    public void aPetWithIDAndNameGoodBoyAndStatusAvailableIsCreated(int petID) {
        petinformationSteps.createPet(petID);
    }

    @When("a pet with ID '{int}' is requested")
    public void aPetWithIDIsRequested(int petID) {
        petinformationSteps.requestPet(petID);
    }

    @Then("a success response is returned")
    public void aSuccessResponseIsReturned() {
        petinformationSteps.responseShouldBe(200);
    }

    @Then("an error response is returned")
    public void anErrorResponseIsReturned() {
        petinformationSteps.responseShouldBe(404);
    }
}

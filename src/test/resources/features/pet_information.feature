Feature: Manage pet information

  Manage pet information by creating, editing and deleting pet information

  Scenario: Add a new pet to the store
    Given petID '55' does not exist
    When a pet with ID '55' is created
    Then a success response is returned

  Scenario: Request a pet that doesn't exists
    Given petID '55' does not exist
    When a pet with ID '55' is requested
    Then an error response is returned
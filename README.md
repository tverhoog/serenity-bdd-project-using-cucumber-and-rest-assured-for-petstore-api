# Petstore API Test Framework

Using Cucumber, Java, Serenity BDD and Rest Assured

## Description

This framework tests the endpoints of the Petstore demo API of Swagger that can be found at https://petstore.swagger.io/

Cucumber is used to be able to split functional description of the system under test in feature files from the technical implementation of the tests. Serenity
BDD is used to create reusable technical components. Serenity also delivers useful test reports that can be used as living documentation out of the box.
Serenity BDD has Rest Assured embedded as SerenityRest which is used to be able to quickly write components that talk to the API endpoints in a BDD (
given-when-then) way.

## Project structure

```Gherkin
src
  + test
    + java                                      Glue code and test runner
      + petstore                                Model of system under test
        + stepdefinitions                       Definition of order of teststeps
          PetInformationStepDefinitions.java
        + steps                                 Actions and assertions
          PetinformationSteps.java
        CucumberTestSuite.java                  Test runner
        + resources
          + features                            Feature files
            pet_information.feature             Scenarios of functionality
          Serenity.conf                             Project configuration
```

## Installation

- Java 8, Maven and Git are required
- IntelliJ with Cucumber for Java plugin recommended
- Clone the project from https://gitlab.com/tverhoog/serenity-bdd-project-using-cucumber-and-rest-assured-for-petstore-api

## Run

Use the following command to run all tests
```mvn clean verify```

### Run specific scenario

1. Add a tag to a scenario such as @runthis
1. Start the project using the command `mvn clean verify -Dcucumber.filter.tags=" @runthis"`

## Write new tests

To test more endpoints and functionality of the Swagger petstore follow these steps

1. Add a feature file with a name and description of the feature or endpoint
1. Write descriptive scenarios for each positive and negative scenario that needs to be automated
1. Let IntelliJ generate step definition files
1. Reuse steps where possible
1. Add steps where needed

## Reports

After local excecution reports will be written to `target/site/serenity/index.html`
After excecution in GitLab (after every commit) reports will be saved as artefact in https://gitlab.com/tverhoog/serenity-bdd-project-using-cucumber-and-rest-assured-for-petstore-api/-/pipelines

## Endpoint interactions

Endpoint interactions are documented at https://petstore.swagger.io/
Interactions of this project with the API are written in the feature files.